# Overview:
* Given an object and an image, the software detects the position of the object in the image if it exists.
* Implementation of the SIFT methods as described by David G. Lowe in his paper "Object Recognition from Local Scale-Invariant Features"
* This method is invariant by image scaling, translation, and rotation, and partially invariant to illumination changes and affine or 3D projection.
* Used languages and technologies: C++ | Python | CMake | Git.


# Requirements:
- OpenCV

# To run the project
> chmod +x configure && ./configure

# To generate the documentation
> make doc

# Project description:
In this project, we implement an Object Software Detection.
To accomplish this task, the project is divided in 2 parts:
- Feature Extraction using SIFT method (http://www.cs.ubc.ca/~lowe/keypoints/) (Implemented from scratch).
- Matching using SVM (with scikit-learn).

* Feature Extraction with SIFT:
SIFT method is invariant by Scale / Rotation / 3D View Point / Noise / Illumination.
It has a near real time performance.
This method has 4 big parts:
- Detection of interest points.
- Keypoint localization (Selected based on their stability)
- Orientation Assignment (one or multiple orientations are assigned to each keypoint)
- Keypoint descriptors (A descriptor of size 128 (4 * 4 * 8) is assigned to each keypoint. Each descriptor is rotated depending on the keypoint orientation(s))
