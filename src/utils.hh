#pragma once

#include <opencv2/core/core.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <string>


// Colors
const char* RED_COLOR="\033[1;31m";
const char* GREEN_COLOR="\033[1;32m";
const char* YELLOW_COLOR="\033[1;33m";
const char* RESET_COLOR="\033[0m";
// Detector constants
const int kOctaveLvl = 4;
const int kScaleLvl = 5;
const int kDogLvl = 4;
const float kSigma = 1.6;
const float kDetector = sqrt(2);
