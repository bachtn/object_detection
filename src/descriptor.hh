#pragma once

#include <iostream>
#include <cmath>
#include <vector>

#include "detector.hh"
#include "keypoint.hh"

class Descriptor {

  public:
    Descriptor(int x_coordinate, int y_coordinate, double orinetation, std::vector<double> descriptor_histogram);
    std::pair<int, int> get_keypointCoordinates();
    std::pair<int, int> get_keypointCoordinates() const;
    std::vector<double> get_descriptorHistogram();
    std::vector<double> get_descriptorHistogram() const;

  private:
      int x_coordinate_;
      int y_coordinate_;
      double orientation_;
      // A vector of 4*4*8: 8 = a histogram of 8 orinetations
      std::vector<double> descriptor_histogram_;
};
