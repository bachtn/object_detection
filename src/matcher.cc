#include "matcher.hh"

Matcher::Matcher(std::vector<Descriptor> target_image_descriptors)
  : target_image_descriptors_(target_image_descriptors)
{}

std::vector<std::pair<int, int>> Matcher::matchImage(const cv::Mat& image) {
  std::cout << "\033[1;32m" << "- Computing Matching descriptors" << "\033[0m" << std::endl;
  std::vector<std::pair<int, int>> matched_points;
  auto test_descriptors = getImageDescriptors(image);
  auto current_idx = -1;
  auto nb_matched = 0;
  auto nb_desc_zero_distance = 0;
  # pragma omp parallel for simd
  for (size_t idx = 0; idx < test_descriptors.size(); ++idx) {
    auto test_descriptor = test_descriptors[idx];
    std::cout << "\033[1;32m" << "- current = " << "\033[1;33m" << current_idx 
              << "\033[1;32m" << ", final = " << "\033[1;33m" << test_descriptors.size()
              << "\033[1;32m" << ", matched = " << "\033[1;33m" << nb_matched 
              << "\033[1;32m" << ", descriptor with zero distance = " << "\033[1;33m" << nb_desc_zero_distance
              << "\033[0m" << std::endl;
    ++current_idx;
    double closest_distance = std::numeric_limits<int>::max();
    double second_closest_distance = std::numeric_limits<int>::max();
    int closest_idx = -1, second_closest_idx = -1;
    for (auto train_descriptor : target_image_descriptors_) {
      auto current_distance = getDistanceBetweenTwoVectors(
          test_descriptor.get_descriptorHistogram(), train_descriptor.get_descriptorHistogram()
          );
      auto new_values = getNewValues(closest_distance, second_closest_distance, current_distance,
          closest_idx, second_closest_idx, current_idx);
      closest_distance = std::get<0>(new_values);
      closest_idx = std::get<1>(new_values);
      second_closest_distance = std::get<2>(new_values);
      second_closest_idx = std::get<3>(new_values);
    }
    if (closest_distance == 0 || second_closest_distance == 0)  {
      ++nb_desc_zero_distance;
    }
    if (closest_distance / second_closest_distance < 0.8) {
      ++nb_matched;
      auto closest_descriptor = test_descriptors[closest_idx];
      // TODO: when you take the keypoints, determine the new coordianetes in the original image,
      //depending on the octave level! Important when drawing the image
      matched_points.push_back(closest_descriptor.get_keypointCoordinates());
    }
  }
  std::cout << "\033[1;32m" << "- Number of matched points = "
            << "\033[1;33m" << matched_points.size() << "\033[0m" << std::endl;
  std::cout << "\033[1;32m" << "- Number of descriptors with distance zero = "
            << nb_desc_zero_distance << "\033[0m" << std::endl;
  return matched_points;
}

double Matcher::getDistanceBetweenTwoVectors(const std::vector<double>& vect1, const std::vector<double>& vect2) {
  double d = 0;
  if (vect1.size() != vect2.size()) return std::numeric_limits<int>::max();
  for (size_t i = 0; i < vect1.size(); ++i) d += pow(vect1[i] - vect2[i], 2);
  return sqrt(d);
}

// returns the new distances and the new indexs
std::tuple<double, int, double, int> Matcher::getNewValues(double closest_distance, double second_closest_distance, double current_distance,
    int closest_idx, int second_closest_idx, int current_idx) {
  if (current_distance < closest_distance) {
    second_closest_distance = closest_distance;
    second_closest_idx = closest_idx;
    closest_distance = current_distance;
    closest_idx = current_idx;
  } else if (current_distance < second_closest_distance) {
    second_closest_distance = current_distance;
    second_closest_idx = current_idx;
  }
  return std::make_tuple(closest_distance, closest_idx, second_closest_distance, second_closest_idx);
}

std::vector<Descriptor> Matcher::getImageDescriptors(const cv::Mat& image) {
  return DescriptorUtils(
      Detector(4, 5, 4, 1.6, sqrt(2), image)
      ).get_keypointsDescriptors();
}

void Matcher::drawMatchedRegion(cv::Mat image, std::vector<std::pair<int, int>> matched_points) {
  std::cout << "\033[1;32m" << "- Drawing matched region" << "\033[0m" << std::endl;
  // Determine the min / max points coordinates
  auto min_x = image.cols - 1, min_y = image.rows - 1, max_x = 0, max_y = 0;
  for (auto point : matched_points) {
    if (point.first < min_x) min_x = point.first;
    if (point.second < min_y) min_y = point.second;
    if (point.first > max_x) max_x = point.first;
    if(point.second > max_y) max_y = point.second;
  }

  for (auto point :  matched_points) {
    cv::rectangle(image, cv::Point(point.first, point.second), cv::Point(point.first + 16, point.second + 16), cv::Scalar(0, 0, 255));
  }

  // Draw rectangle
  std::cout << "min x = " << min_x << ", min y = " << min_y << ", max x = " << max_x << ", max y = " << max_y << std::endl;
  cv::rectangle(image, cv::Point(min_x, min_y), cv::Point(max_x - min_x, max_y - min_y), cv::Scalar(0, 0, 255));
  // Save image
  auto fname = "samples/output/matchedImage.jpg";
  imwrite(fname, image);
  // Display image
  cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);
  cv::imshow("Display window", image);
  cv::waitKey(0);
  cv::destroyAllWindows();
}
