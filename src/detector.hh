#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>

#include "keypoint.hh"

class Detector {
  public:
    Detector();
    Detector(int octave_lvl, int scale_lvl, int dog_lvl, float sigma, float k, cv::Mat initial_image);
    // getters
    std::vector<std::vector<cv::Mat>> get_dogPyramid();
    std::vector<KeyPoint> get_keypointVec();
    float get_sigma();
    float get_k();
    cv::Mat get_image(int octave_lvl, int dog_lvl);

    cv::Mat grayscale(cv::Mat image);
    // Creates a pyramid with Gaussian Blur Filter
    std::vector<std::vector<cv::Mat>> getGaussPyramid(const cv::Mat &image);
    // Creates a pyramid with Differance of Gauss
    std::vector<std::vector<cv::Mat>> getDogPyramid(const std::vector<std::vector<cv::Mat>> &gauss_pyramid);
    //************** Localize interest points
    std::pair<std::vector<KeyPoint>, std::vector<cv::Mat>> localizeInterestPoints();
  private:
    bool isKeypoint(const cv::Mat &current_image, int x_coordinate, int y_coordinate, int octave_lvl, int dog_lvl);
    bool isMaxima(float pixel, const cv::Mat &image, int x_coordinate, int y_coordinate, bool same_img);
    bool isMinima(float pixel, const cv::Mat &image, int x_coordinate, int y_coordinate, bool same_img);
    bool passEdgeResponse(int x_coordinate, int y_coordinate, const cv::Mat& current_image);
  private:
    int octave_lvl_;
    int scale_lvl_;
    int dog_lvl_;
    float sigma_;
    float k_;
    cv::Mat initial_image_;
    std::vector<std::vector<cv::Mat>> dog_pyramid_; //TODO: replace vecto(vector) by only one vector
    std::vector<KeyPoint> keypoint_vec_;
};

