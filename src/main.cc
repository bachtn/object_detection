#include "utils.hh"
#include "detector.hh"
#include "descriptor-utils.hh"
#include "keypoint.hh"
#include "descriptor.hh"
#include "matcher.hh"

/*! \fn std::tuple<int, std::vector<cv::Mat>, cv::Mat> getImageList(int argc, char** argv)
 * \brief Validates the passed arguments and creates a list of the target images and the test image.
 * * If an error occurs (invalid image file), the validation status will be set to 0 otherwise 1.
 * \param argc The number of passed arguments
 * \param argv The list of the passed arguments
 * \return A tuple of the validation status, vector of training images and the test image
 */
std::tuple<int, std::vector<cv::Mat>, cv::Mat> getImageList(int argc, char** argv);

/*! \fn std::vector<Descriptor> getTrainDescriptors(const std::vector<cv::Mat>& init_img_list);
 * \brief Creates a list of descriptors of each image in the argument list.
 * * Detects the keypoints of each image, filters them and cretes their descriptors.
 * * These descriptors will used to detect the desired object in the test images.
 * \param init_img_list The list of training images given by the user
 * \return a vector of the training images 
 */
std::vector<Descriptor> getTrainDescriptors(const std::vector<cv::Mat>& init_img_list);

/*! \fn void display(std::vector<cv::Mat> &img_list);
 * \brief Displays the list of images passed in argument.
 * \param img_list the images to display.
 * \return void
 */
void display(std::vector<cv::Mat> &img_list);

int main (int argc, char** argv) {
  // validate passed arguments
  if (argc < 3) {
    std::cout
      << YELLOW_COLOR << "* Usage: You should give 2 arguments:" << std::endl
      << "- 1 - a list of images of the object to detect" << std::endl
      << "- 2 - the test image" 
      << RESET_COLOR << std::endl;
    return -1;
  }

  // validate passed images
  auto aux = getImageList(argc, argv);
  if (std::get<0>(aux) == 0) return -1;
  auto train_img_list = std::get<1>(aux);
  auto test_img = std::get<2>(aux);

  // Compute train descriptors
  auto train_keypoint_descriptors = getTrainDescriptors(train_img_list);

  // Match the train and test descriptors to locate the object in the test image if it exits
  auto matcher = Matcher(train_keypoint_descriptors);
  auto matched_keypoints = matcher.matchImage(test_img.clone());
  
  // Draw the object loation if it exists
  matcher.drawMatchedRegion(test_img, matched_keypoints);

  return 0;
}


std::tuple<int, std::vector<cv::Mat>, cv::Mat> getImageList(int argc, char** argv) {
  auto error = 1;
  std::vector<cv::Mat> train_img_list;
  for (auto i = 1; i < argc - 1; ++i) {
    auto img = cv::imread(argv[i], CV_LOAD_IMAGE_COLOR);   // Read the file
    if (!img.data) {
      std::cerr << RED_COLOR << "- Could not open or find the image " << argv[i]
                << RESET_COLOR << std::endl;
      error = 0;
    } else {
      std::cout << GREEN_COLOR << "- Reading image " << YELLOW_COLOR << argv[i] 
                << RESET_COLOR << std::endl;
      train_img_list.push_back(img);
    }
  }

  auto test_img = cv::imread(argv[argc - 1], CV_LOAD_IMAGE_COLOR);   // Read the file
  if (!test_img.data) {
    std::cerr << RED_COLOR << "- Could not open or find the image " << argv[argc - 1]
              << RESET_COLOR << std::endl;
    error = 0;
  }

  return std::make_tuple(error, train_img_list, test_img);
}

std::vector<Descriptor> getTrainDescriptors(const std::vector<cv::Mat>& init_img_list) {
  std::vector<Descriptor> train_descriptors;
  for (auto img : init_img_list) {
    auto detector = Detector(kOctaveLvl, kScaleLvl, kDogLvl, kSigma, kDetector, img);
    auto descriptor_utils = DescriptorUtils(detector);
    auto keypoints_descriptors = descriptor_utils.get_keypointsDescriptors();
    train_descriptors.insert(train_descriptors.end(), keypoints_descriptors.begin(), keypoints_descriptors.end());
  }
  
  std::cout << GREEN_COLOR << "- number of Train descriptors = " << YELLOW_COLOR
            << train_descriptors.size() << RESET_COLOR << std::endl;
  return train_descriptors;
}

void display(std::vector<cv::Mat> &img_list) {
  for (auto image : img_list) {
    cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);// Create a window for display.
    cv::imshow("Display window", image);    // Show our image inside it.
    cv::waitKey(0);  // Wait for a keystroke in the window
    cv::destroyAllWindows();
  }
}
