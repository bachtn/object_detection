#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>
#include <limits>

#include "detector.hh"
#include "descriptor.hh"
#include "descriptor-utils.hh"
#include "keypoint.hh"

class Matcher {
  public:
    Matcher(std::vector<Descriptor> target_image_descriptors);
    /* Takes an image, extractcs its, descriptors, match them with the referance
     * object descriptors and returns the coordinates of the matched points */
    std::vector<std::pair<int, int>> matchImage(const cv::Mat& image);
    void drawMatchedRegion(cv::Mat image, std::vector<std::pair<int, int>> matched_points);

  private:
    std::vector<Descriptor> getImageDescriptors(const cv::Mat& image);
    std::tuple<double, int, double, int> getNewValues(
        double closest_distance, double second_closest_distance, double current_distance,
        int closest_idx, int second_closest_idx, int current_idx);
    double getDistanceBetweenTwoVectors(const std::vector<double>& vect1, const std::vector<double>& vect2);
  private:
    std::vector<Descriptor> target_image_descriptors_;


};
