#include "keypoint.hh"

KeyPoint::KeyPoint(int xCoordinate, int yCoordinate, int octaveLvl, int dogLvl, float sigma)
  : xCoordinate_(xCoordinate)
  , yCoordinate_(yCoordinate)
  , octaveLvl_(octaveLvl)
  , dogLvl_(dogLvl)
  , sigma_(sigma)
{}

int KeyPoint::get_xCoordinate() { return xCoordinate_; }
int KeyPoint::get_xCoordinate() const { return xCoordinate_; }
int KeyPoint::get_yCoordinate() { return yCoordinate_; }
int KeyPoint::get_yCoordinate() const { return yCoordinate_; }
int KeyPoint::get_octaveLvl() { return octaveLvl_; }
int KeyPoint::get_octaveLvl() const { return octaveLvl_; }
int KeyPoint::get_dogLvl() { return dogLvl_; }
int KeyPoint::get_dogLvl() const { return dogLvl_; }
float KeyPoint::get_sigma() { return sigma_; }
float KeyPoint::get_sigma() const { return sigma_; }
