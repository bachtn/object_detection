#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>

#include "detector.hh"
#include "descriptor.hh"
#include "keypoint.hh"

class DescriptorUtils {
  public:
    // Constructors
    DescriptorUtils(const Detector& detector);
    //Getters
    std::vector<Descriptor> get_keypointsDescriptors();
    std::vector<Descriptor> get_keypointsDescriptors() const;
    /*! \fn std::vector<double> DescriptorUtils::getOrientationHistogram(
     * const KeyPoint& keypoint, const cv::Mat& current_image, int nbrDirections, int windowSize)
     *  \brief Takes a keypoint and makes its orientation histogram.
     *  * The orientation histogram is composed of 36 bins, each bin represents an interval
     *  of size 10 degrees (1st bin = 0 degree - 10 degree, 2nd bin = 11 degree - 20 degree,
     *  final bin (nbr 36) = interval between 351 degree - 359 degree)
     *  * Each element in the orientation histogram represents the sum of all the weightd magnitude
     *  gradients with orientation that belongs to the bin interval.
     *  * The magnitudes are weighted with a gaussian 
     *  \param keypoint The target keypoint.
     *  \param current_image the target image
     *  \param nbrDirections number of directioons (if its a 36 bin histogram than = 36)
     *  (will be 36 for orientation and 8 for descriptor)
     *  \param windowSize the square where the keyPoint is in the middle
     *  (will be 16 for orientation and 4 for descriptor)
     *  \return a double vector with size = nbrDirections and values = magnitude
     */
    std::vector<double> getOrientationHistogram(const KeyPoint& keypoint, const cv::Mat& current_image,
        int nbrDirections, int windowSize, bool is_descriptor);
    /*! \fn std::vector<double> normalizeDescriptor(std::vector<double>& descriptor);
     *  \brief Sets the values greater than 0.2 to 0.2
     *  \param descriptor to normalize
     *  \return normalized descriptor with 0.2 as max value for elements
     */
    std::vector<double> normalizeDescriptor(std::vector<double>& descriptor);
    /*! \fn std::vector<double> normalizeVector(std::vector<double>& vector_to_normalize);
     *  \brief normalize a 1d vector by dividing the vector norm from each element
     *  \param the vecor to normalize
     *  \return the normalized vector with values between 0 and 1
     */
    std::vector<double> normalizeVector(std::vector<double>& vector_to_normalize);
    /*! \fn double euclideanNorm(const std::vector<double>& vect);
     *  \brief Computes the euclideanNorm of a 1d vector
     *  \param vect double vector containing the magnitudes of the gradients of a descriptor
     *  \return double
     */
    double euclideanNorm(const std::vector<double>& vect);
    /*! \fn std::vector<double> getKeypointOrientations(const std::vector<double>& orientation_histogramme);
     *  \brief 
     *  \param 
     *  \return 
     */
    std::vector<double> getKeypointOrientations(const std::vector<double>& orientation_histogramme);
    /*! \fn std::vector<double> getKeypointDescriptor(const KeyPoint& keypoint, const cv::Mat& current_image);
     *  \brief 
     *  \param 
     *  \return 
     */
    std::vector<double> getKeypointDescriptor(const KeyPoint& keypoint, const cv::Mat& current_image);
    /*! \fn std::vector<Descriptor> getKeypointDescriptors(const KeyPoint& keypoint);
     *  \brief 
     *  \param 
     *  \return 
     */
    std::vector<Descriptor> getKeypointDescriptors(const KeyPoint& keypoint);
    /*! \fn std::vector<Descriptor> getKeypointsDescriptors();
     *  \brief 
     *  \param 
     *  \return 
     */
    std::vector<Descriptor> getKeypointsDescriptors();
    /*! \fn std::pair<double, double> getGradMagnitudeOrientation(const cv::Mat &dog_image, int x, int y);
     *  \brief 
     *  \param 
     *  \return 
     */
    std::pair<double, double> getGradMagnitudeOrientation(const cv::Mat &dog_image, int x, int y);
    /*! \fn std::vector<double> getGaussianWeights(const int octaveLvl, const int dog_lvl, const int window_radius, bool is_descriptor);
     *  \brief 
     *  \param 
     *  \return 
     */
    std::vector<double> getGaussianWeights(const int octaveLvl, const int dog_lvl, const int window_radius, bool is_descriptor);
    
    /*! \fn 
     *  \brief 
     *  \param 
     *  \return 
     */


  private:
    Detector detector_;
    std::vector<Descriptor> keypoints_descriptors_;
};
