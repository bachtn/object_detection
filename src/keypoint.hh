#pragma once

#include <iostream>

class KeyPoint {
  public:
    KeyPoint(int xCoordinate, int yCoordinate, int octaveLvl, int dogLvl, float sigma);
    int get_xCoordinate();
    int get_xCoordinate() const;
    int get_yCoordinate();
    int get_yCoordinate() const;
    int get_octaveLvl();
    int get_octaveLvl() const;
    int get_dogLvl();
    int get_dogLvl() const;
    float get_sigma();
    float get_sigma() const;

  private:
    int xCoordinate_;
    int yCoordinate_;
    int octaveLvl_;
    int dogLvl_;
    float sigma_;
};
