#include "descriptor-utils.hh"

// Constructors
DescriptorUtils::DescriptorUtils(const Detector& detector) {
  this->detector_ = detector;
  std::cout << "\033[1;32m" << "- Creating keypoint descriptors..."
            << "\033[0m" << std::endl;
  this->keypoints_descriptors_ = getKeypointsDescriptors();
  std::cout << "\033[1;32m" << "- Number of descriptors = "
            << "\033[1;33m" << this->keypoints_descriptors_.size()
            << "\033[0m" << std::endl;
}

// Getters
std::vector<Descriptor> DescriptorUtils::get_keypointsDescriptors() {
  return keypoints_descriptors_;
}
std::vector<Descriptor> DescriptorUtils::get_keypointsDescriptors() const {
  return keypoints_descriptors_;
}

// Other methods

/*! 
 * \brief Returns a normalized vector of all descriptors
 */
std::vector<Descriptor> DescriptorUtils::getKeypointsDescriptors() {
  std::vector<Descriptor> keypoints_descriptors;
  auto keypoint_vec = detector_.get_keypointVec();
  for (auto keypoint : keypoint_vec) {
    auto current_keypoint_descriptors = getKeypointDescriptors(keypoint);
    keypoints_descriptors.insert(
        keypoints_descriptors.end(), current_keypoint_descriptors.begin(),
        current_keypoint_descriptors.end()
        );
  }
  return keypoints_descriptors;
}

std::vector<Descriptor> DescriptorUtils::getKeypointDescriptors(const KeyPoint& keypoint) {
  std::vector<Descriptor> keypoint_descriptors;
  auto current_image = detector_.get_image(keypoint.get_octaveLvl(), keypoint.get_dogLvl());
  auto orientation_histogramme = getOrientationHistogram(keypoint, current_image, 36, 16, false);
  auto keypoint_orientations = getKeypointOrientations(orientation_histogramme);
  auto keypoint_descriptor = getKeypointDescriptor(keypoint, current_image);
  auto normalizedKeypointDescriptor = normalizeDescriptor(keypoint_descriptor);
  for (auto keypoint_orientation : keypoint_orientations) {
    auto newDescriptor = Descriptor(keypoint.get_xCoordinate(), keypoint.get_yCoordinate(),
          keypoint_orientation, normalizedKeypointDescriptor);
    keypoint_descriptors.push_back(newDescriptor);
  }
  return keypoint_descriptors;
}

std::vector<double> DescriptorUtils::getKeypointOrientations(const std::vector<double>& orientation_histogramme) {
  std::vector<double> keypoint_orientations;
  auto highestPeak = *std::max_element(orientation_histogramme.begin(), orientation_histogramme.end());
  for (size_t i = 0; i < orientation_histogramme.size(); ++i) {
    if (orientation_histogramme[i] >= highestPeak * 0.8) { // The magnitude that are > 80% highestPeak
      keypoint_orientations.push_back(i);
    }
  }
  return keypoint_orientations;
}

std::vector<double> DescriptorUtils::getKeypointDescriptor(const KeyPoint& keypoint, const cv::Mat& current_image) {
  std::vector<double> keypoint_descriptor;
  for (int y_desc = 0; y_desc < 4; ++y_desc) {
    for (int x_desc = 0; x_desc < 4; ++x_desc) {
      int x_coord = keypoint.get_xCoordinate() - 6 + 4 * x_desc;
      int y_coord = keypoint.get_yCoordinate() - 6 + 4 * y_desc;
      auto sub_desc_keypoint = KeyPoint(x_coord, y_coord, keypoint.get_octaveLvl(), keypoint.get_dogLvl(), keypoint.get_sigma());
      auto ori_hist = getOrientationHistogram(sub_desc_keypoint, current_image, 8, 4, true);
      keypoint_descriptor.insert(keypoint_descriptor.end(), ori_hist.begin(), ori_hist.end());
    }
  }
  return keypoint_descriptor;
}

std::vector<double> DescriptorUtils::getOrientationHistogram(const KeyPoint& keypoint, const cv::Mat& current_image,
    int nb_directions, int window_size, bool is_descriptor) { // 
  std::vector<double> ori_hist(nb_directions, 0.f); // size = nb_directions = 36 bins or 8 bins
  //Get the gaussian weights
  auto gaussian_weights  = getGaussianWeights(keypoint.get_octaveLvl(), keypoint.get_dogLvl(), window_size / 2, is_descriptor);
  int x_coord = keypoint.get_xCoordinate(), y_coord = keypoint.get_yCoordinate();
  for (int y = 0; y < window_size; ++y ) {
    for (int x = 0; x < window_size; ++x) {
      auto gradient_mag_ori = getGradMagnitudeOrientation(current_image, x_coord - window_size / 2 + x , y_coord - window_size / 2 + y);
      auto weight = gaussian_weights[x + window_size * y];
      auto magnitude = gradient_mag_ori.first * weight;
      auto orientation =  gradient_mag_ori.second;
      auto index =  orientation == 360 ? 0 : (int) floor(orientation * nb_directions / 360);
      //TODO: replace orientation by orientation - keypoint_orientation -> for rotation invariance
      ori_hist.at(index) += magnitude;
    }
  }
  return ori_hist;
}

std::pair<double, double> DescriptorUtils::getGradMagnitudeOrientation(const cv::Mat &dog_image, int x, int y) {
  auto dx = dog_image.at<uchar>(y,x + 1) - dog_image.at<uchar>(y,x - 1);
  auto dy = dog_image.at<uchar>(y + 1,x) - dog_image.at<uchar>(y - 1,x);
  auto magnitude = sqrt(pow(dx, 2) + pow(dy, 2));
  auto tmp = (180 / M_PIl) * atan2(dy, dx);
  auto orientation = tmp > 0 ? tmp : tmp + 360;
  return std::make_pair(magnitude, orientation);
}

std::vector<double> DescriptorUtils::getGaussianWeights(const int octave_lvl, const int dog_lvl, const int window_radius, bool is_descriptor) {
  std::vector<double> weights;
  for (int y = - window_radius; y < window_radius; ++y)
    for (int x = - window_radius; x < window_radius; ++x) {
      auto keypoint_scale = detector_.get_sigma() * pow(detector_.get_k(), 2 * octave_lvl + dog_lvl);
      auto sigma_weight = is_descriptor ? 2 : keypoint_scale * 1.5; // Changed descriptor sigma to 2 au lieu de 8
      weights.push_back(std::exp((pow(x,2) + pow(y, 2)) / (-2 * pow(sigma_weight, 2))) / (2 * M_PIl * pow(sigma_weight, 2)));
    }
  return weights;
}

std::vector<double> DescriptorUtils::normalizeDescriptor(std::vector<double>& descriptor) {
  descriptor = normalizeVector(descriptor);
  // Set all magnitude with value > 0.2 to 0.2 (as in the SIFT search paper)
  for (auto& component : descriptor)
    component = component > 0.2 ? 0.2 : component;
  return normalizeVector(descriptor);
}

std::vector<double> DescriptorUtils::normalizeVector(std::vector<double>& vector_to_normalize) {
  auto vectorNorm = euclideanNorm(vector_to_normalize);
  if (vectorNorm != 0) {
    for (auto& component : vector_to_normalize) {
      component /= vectorNorm;
    }
  }
  return vector_to_normalize;
}

double DescriptorUtils::euclideanNorm(const std::vector<double>& vect) {
  double vectorNorm = 0;
  for (auto component : vect)
    vectorNorm += component * component;
  return sqrt(vectorNorm);
}
