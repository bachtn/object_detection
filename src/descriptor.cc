#include "descriptor.hh"

Descriptor::Descriptor(int x_coordinate, int y_coordinate, double orientation, std::vector<double> descriptor_histogram)
  : x_coordinate_(x_coordinate)
  , y_coordinate_(y_coordinate)
  , orientation_(orientation)
  , descriptor_histogram_(descriptor_histogram)
{}

std::pair<int, int> Descriptor::get_keypointCoordinates() {
  return std::make_pair(x_coordinate_, y_coordinate_);
}
std::pair<int, int> Descriptor::get_keypointCoordinates() const {
  return std::make_pair(x_coordinate_, y_coordinate_);
}

std::vector<double> Descriptor::get_descriptorHistogram() { return descriptor_histogram_; }
std::vector<double> Descriptor::get_descriptorHistogram() const { return descriptor_histogram_; }
