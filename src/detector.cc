#include "detector.hh"

// Constructors
Detector::Detector() {}
Detector::Detector(int octave_lvl, int scale_lvl, int dog_lvl, float sigma, float k, cv::Mat initial_image) { //TODO: remove dog_lvl
  this->octave_lvl_ = octave_lvl;
  this->scale_lvl_ = scale_lvl;
  this->dog_lvl_ = dog_lvl;
  this->sigma_ = sigma;
  this->k_ = k;
  this->initial_image_ = initial_image;
  std::cout << "\033[1;32m" << "- Building DoG pyramid..." << "\033[0m" << std::endl;
  auto gray_image = grayscale(initial_image);
  auto gauss_pyramid = getGaussPyramid(gray_image);
  this->dog_pyramid_ = getDogPyramid(gauss_pyramid);
  std::cout << "\033[1;32m" << "- Localizing keypoints..." << "\033[0m" << std::endl;
  this->keypoint_vec_ = localizeInterestPoints().first;
  std::cout << "\033[1;32m" << "- Number of keypoints = " << "\033[1;33m" 
            << this->keypoint_vec_.size() << "\033[0m" << std::endl;
}

// Getters
std::vector<std::vector<cv::Mat>> Detector::get_dogPyramid() { return dog_pyramid_; }
std::vector<KeyPoint> Detector::get_keypointVec() { return keypoint_vec_; }
float Detector::get_sigma() { return sigma_; }
float Detector::get_k() { return k_; }
cv::Mat Detector::get_image(int octave_lvl, int dog_lvl) { return dog_pyramid_[octave_lvl][dog_lvl]; }

// Other Methods
cv::Mat Detector::grayscale(cv::Mat image) {
  cv::Mat gray_img;
  cv::cvtColor(image, gray_img, cv::COLOR_RGB2GRAY);
  return gray_img;
}

std::vector<std::vector<cv::Mat>> Detector::getGaussPyramid(const cv::Mat &image) {
  std::vector<std::vector<cv::Mat>> gauss_pyramid;
  for (int oct_lvl = 0; oct_lvl < octave_lvl_; ++oct_lvl) {
    // Get Octave 1st image
    cv::Mat dst1;
    auto new_f = pow(2, -1*oct_lvl);
    cv::resize(image, dst1, cv::Size(), new_f, new_f, cv::INTER_CUBIC);
    // Create Octave container
    std::vector<cv::Mat> octave;
    //octave.push_back(dst1);
    for (int i = 1; i < scale_lvl_ + 1; ++i) {
      cv::Mat dst2;
      auto fx = sigma_ * pow(2,oct_lvl) * pow(k_,i);
      cv::GaussianBlur(dst1, dst2, cv::Size(), fx, 0);
      octave.push_back(dst2);
    }
    gauss_pyramid.push_back(octave);
  }
  return gauss_pyramid;
}

std::vector<std::vector<cv::Mat>> Detector::getDogPyramid(const std::vector<std::vector<cv::Mat>> &gauss_pyramid) {
  std::vector<std::vector<cv::Mat>> dog_pyramid;
  for (auto gauss_octave : gauss_pyramid) {
    std::vector<cv::Mat> octave;
    for (int dog_lvl = 0; dog_lvl < dog_lvl_; ++dog_lvl) {
      octave.push_back(gauss_octave[dog_lvl+1] - gauss_octave[dog_lvl]);
    }
    dog_pyramid.push_back(octave);
  }
  return dog_pyramid;
}
/*
 * Returns a pair of vecors : keypoint_vec, img_with_keypoints_vec
 * keypoint_vec = contains the list of all localized keypoints
 * img_with_keypoints_vec = contains a list of images with the keypoints
 *  drawn on with the white color
 */
std::pair<std::vector<KeyPoint>, std::vector<cv::Mat>> Detector::localizeInterestPoints() {
  std::vector<KeyPoint> keypoint_vec;
  std::vector<cv::Mat> img_with_keypoints_vec;
  //for (auto octave : dog_pyramid) {
  for (int octave_lvl = 0; octave_lvl < octave_lvl_; ++octave_lvl) {
    for (int dog_lvl = 1; dog_lvl < dog_lvl_ - 1; ++dog_lvl) {
      //TODO: handle the case where i and j == 0 or i and j == width, height
      auto current_img = dog_pyramid_[octave_lvl][dog_lvl];
      auto img_with_keypoints = dog_pyramid_[octave_lvl][dog_lvl].clone();
      for(int y_coordinate = 1; y_coordinate < current_img.rows - 1; ++y_coordinate) {
        for (int x_coordinate = 1; x_coordinate < current_img.cols - 1; ++x_coordinate) {
          if (isKeypoint(current_img, x_coordinate, y_coordinate, octave_lvl, dog_lvl)) {
            //TODO: change sigma of the keypoint to the correct sigma
            keypoint_vec.push_back(KeyPoint(x_coordinate, y_coordinate, octave_lvl, dog_lvl, sigma_));
            img_with_keypoints.at<uchar>(y_coordinate, x_coordinate) = 255;
          }
        }
      }
      img_with_keypoints_vec.push_back(img_with_keypoints);
    }
  }
  return std::make_pair(keypoint_vec, img_with_keypoints_vec);
}

bool Detector::isKeypoint(const cv::Mat &current_img, int x_coordinate, int y_coordinate, int octave_lvl, int dog_lvl) {
  auto pixel = (int)current_img.at<uchar>(y_coordinate, x_coordinate);
  bool isMax = isMaxima(pixel, current_img, x_coordinate, y_coordinate, true)
    && isMaxima(pixel, dog_pyramid_[octave_lvl][dog_lvl-1], x_coordinate, y_coordinate, false)
    && isMaxima(pixel, dog_pyramid_[octave_lvl][dog_lvl+1], x_coordinate, y_coordinate, false);
  bool isMin = isMinima(pixel, current_img, x_coordinate, y_coordinate, true)
    && isMinima(pixel, dog_pyramid_[octave_lvl][dog_lvl-1], x_coordinate, y_coordinate, false)
    && isMinima(pixel, dog_pyramid_[octave_lvl][dog_lvl+1], x_coordinate, y_coordinate, false);
  return (
      (isMax || isMin) && passEdgeResponse(x_coordinate, y_coordinate, current_img)
      ) ? true : false;
}

// Eliminate extremas (points) that are sensitive to edge translation (not near a corner)
bool Detector::passEdgeResponse(int x, int y, const cv::Mat& current_img) {
  auto pixel_value = (float)current_img.at<uchar>(y,x);

  auto x1 = (float)current_img.at<uchar>(y,x+1);
  auto x0 = (float)current_img.at<uchar>(y,x-1);
  auto y1 = (float)current_img.at<uchar>(y+1,x);
  auto y0 = (float)current_img.at<uchar>(y-1,x);
  auto xy1 = (float)current_img.at<uchar>(y+1,x+1);
  auto xy0 = (float)current_img.at<uchar>(y-1,x-1);
  auto x0y1 = (float)current_img.at<uchar>(y+1,x-1);
  auto x1y0 = (float)current_img.at<uchar>(y-1,x+1);
  // Second Derivative X
  double d_xx = x1 + x0 - 2 * pixel_value;
  // Second Derivative Y
  double d_yy = y1 + y0 - 2 * pixel_value;
  // Second Derivative XY
  double d_xy = xy1 + xy0 - x0y1 - x1y0;

  // Trace of the matrix (sum of the eigenvalues)
  double tr_h = d_xx + d_yy;

  //Determinant of the Hessian matrix H
  double det_h = d_xx * d_yy - d_xy * d_xy;

  if(det_h <= 0) return false;
  auto r = 10; // In the paper (check Wikipedia)
  auto R = pow(tr_h, 2) / det_h;
  auto rth = pow(r + 1, 2) / r;
  return R < rth;
}

// Check if a given pixel is a maxima
bool Detector::isMaxima(float pixel, const cv::Mat &image, int x, int y, bool same_img) {
  auto result = pixel > (int)image.at<uchar>(y,x+1) && pixel > (int)image.at<uchar>(y,x-1)
      && pixel >= (int)image.at<uchar>(y+1,x) && pixel >= (int)image.at<uchar>(y-1,x)
      && pixel >= (int)image.at<uchar>(y+1,x+1) && pixel >= (int)image.at<uchar>(y-1,x-1)
      && pixel >= (int)image.at<uchar>(y+1,x-1) && pixel >= (int)image.at<uchar>(y-1,x+1);
  result = same_img ? result : result && pixel > (int)image.at<uchar>(y,x);
  return result; 
}

// Check if a given pixel is a minima
bool Detector::isMinima(float pixel, const cv::Mat &image, int x, int y, bool same_img) {
  auto result = pixel < (int)image.at<uchar>(y,x+1) && pixel < (int)image.at<uchar>(y,x-1)
      && pixel <= (int)image.at<uchar>(y+1,x) && pixel <= (int)image.at<uchar>(y-1,x)
      && pixel <= (int)image.at<uchar>(y+1,x+1) && pixel <= (int)image.at<uchar>(y-1,x-1)
      && pixel <= (int)image.at<uchar>(y+1,x-1) && pixel <= (int)image.at<uchar>(y-1,x+1);
  result = same_img ? result : result && pixel < (int)image.at<uchar>(y,x);
  return result;
}
